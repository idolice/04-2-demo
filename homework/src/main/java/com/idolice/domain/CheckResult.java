package com.idolice.domain;

public class CheckResult {
    private Boolean correct;

    public CheckResult(Boolean result) {
        this.correct = result;
    }
    public Boolean getCorrect() {
        return correct;
    }
}
