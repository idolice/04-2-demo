package com.idolice.domain;

import java.util.HashMap;
import java.util.function.BiFunction;

public class OperationMap {
    public static final HashMap<String, BiFunction<Integer, Integer, Boolean>> compareMap
            = new HashMap<String, BiFunction<Integer, Integer, Boolean>>(){{
        put("=", (arg1, arg2) -> arg1 == arg2);
        put(">", (arg1, arg2) -> arg1 > arg2);
        put("<", (arg1, arg2) -> arg1 < arg2);
    }};

    public static final HashMap<String, BiFunction<Integer, Integer, String>> operationMap =
            new HashMap<String, BiFunction<Integer, Integer, String>>(){{
        put("plus", (arg1, arg2) -> String.format("%d+%d=%d ", arg1, arg2, arg1 + arg2));
        put("multiple", (arg1, arg2) -> String.format("%d*%d=%d ", arg1, arg2, arg1 * arg2));
    }};
}
