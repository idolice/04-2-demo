package com.idolice.domain;

import javax.validation.constraints.NotNull;

public class OperationRequest {
    @NotNull
    private Integer operandLeft;
    @NotNull
    private Integer operandRight;
    @NotNull
    private String operation;
    @NotNull
    private Integer expectedResult;
    private String checkType = "=";


    public Integer getOperandLeft() {
        return operandLeft;
    }


    public Integer getOperandRight() {
        return operandRight;
    }


    public String getOperation() {
        return operation;
    }


    public Integer getExpectedResult() {
        return expectedResult;
    }


    public Boolean check() {
        if (operation.equals("*")) {
            return OperationMap.compareMap
                    .get(checkType)
                    .apply(operandLeft * operandRight, expectedResult);
        } else {
            return OperationMap.compareMap
                    .get(checkType)
                    .apply(operandLeft + operandRight, expectedResult);
        }
    }

    public void setCheckType(String checkType) {
        this.checkType = checkType;
    }
}
