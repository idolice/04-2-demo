package com.idolice.util;

import com.idolice.domain.OperationMap;

import java.util.ArrayList;
import java.util.List;

public class MathUtil {

    public static String getTable(Integer start, Integer end, String type) {
        List<String> numbers = new ArrayList<>();
        for (int number = start; number <= end; number++) {
            numbers.add(String.valueOf(number));
        }
        return numbers.stream()
                .reduce("", (total, el) -> generateTableLine(total, el, type));
    }

    private static String generateTableLine(String total, String num, String mathType) {
        Integer value = Integer.parseInt(num);
        StringBuilder stringBuilder = new StringBuilder(total);

        for (Integer i = 1; i <= value; i++) {
            stringBuilder.append(OperationMap.operationMap.get(mathType).apply(value, i));
        }
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }
}
