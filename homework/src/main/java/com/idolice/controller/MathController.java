package com.idolice.controller;

import com.idolice.domain.CheckResult;
import com.idolice.domain.OperationRequest;
import com.idolice.util.MathUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class MathController {
    @GetMapping("/tables/plus")
    public String getPlusMap(@RequestParam(defaultValue = "1") Integer start,
                             @RequestParam(defaultValue = "9") Integer end) {
        return MathUtil.getTable(start, end,"plus");
    }

    @GetMapping("/tables/multiply")
    public String getMultiMap(@RequestParam(defaultValue = "1") Integer start,
                              @RequestParam(defaultValue = "9") Integer end) {
        return MathUtil.getTable(start, end,"multiple");
    }

    @PostMapping("/check")
    public CheckResult check(@RequestBody @Valid OperationRequest operationRequest){
        return new CheckResult(operationRequest.check());
    }

}
